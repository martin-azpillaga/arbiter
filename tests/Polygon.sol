// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "../source/chain/interfaces/Token.sol";
import "../source/chain/interfaces/Exchange.sol";
import "../source/chain/interfaces/LoanProvider.sol";

contract Polygon
{
	LoanProvider public WETH_USDC = LoanProvider(0x5333Eb1E32522F1893B7C9feA3c263807A02d561);

	Token public USDC = Token(0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174);
	Token public DAI = Token(0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063);
	Token public WETH = Token(0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619);
	Token public USDT = Token(0xc2132D05D31c914a87C6611C10748AEb04B58e8F);
	Token public WMATIC = Token(0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270);

	Exchange public SUSHISWAP = Exchange(0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506);
	Exchange public QUICKSWAP = Exchange(0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff);
	Exchange public JETSWAP = Exchange(0x5C6EC38fb0e2609672BDf628B1fD605A523E5923);
	Exchange public POLYCAT = Exchange(0x94930a328162957FF1dd48900aF67B5439336cBD);
	Exchange public APESWAP = Exchange(0xC0788A3aD43d79aa53B09c2EaCc313A787d1d607);
	Exchange public WAULTSWAP = Exchange(0x3a1D87f206D12415f5b0A33E786967680AAb4f6d);
}
