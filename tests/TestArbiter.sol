// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/interfaces/Interfaces.sol";
import "../contracts/Arbiter.sol";
import "../contracts/Polygon.sol";

contract TestArbiter
{
	Arbiter arbiter = Arbiter(DeployedAddresses.Arbiter());
	Polygon polygon = Polygon(DeployedAddresses.Polygon());

	function testLoan() public
	{
		arbiter.loan(polygon.WETH_USDC(), 1000, polygon.USDC(), polygon.WETH(), polygon.SUSHISWAP(), polygon.QUICKSWAP());
	}
}
