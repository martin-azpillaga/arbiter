
const USDT = "0xdAC17F958D2ee523a2206206994597C13D831ec7";
const WETH = "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2";

const USDT_WETH_UNI = "0x0d4a11d5EEaaC28EC3F61d100daF4d40471f1852";
const USDT_WETH_SUSHI = "0x06da0fd433C1A5d7a4faa01111c044910A184553";

const provider = new ethers.providers.WebSocketProvider("wss://eth-mainnet.g.alchemy.com/v2/xVHy4-HSCqPuCYleg7X0kN7y_p1lqH0-");

const factory_interface = ["function getPair(address,address) external view returns (address)"];
const uniswap_factory = new ethers.Contract("0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f", factory_interface, provider);
const sushiswap_factory = new ethers.Contract("0xC0AEe478e3658e2610c5F7A4A2E1777cE9e4f2Ac", factory_interface, provider);

const pair_interface = ["event Sync(uint112, uint112)"];
const uniswap_usdt_weth = new ethers.Contract(USDT_WETH_UNI, pair_interface, provider);
const sushiswap_usdt_weth = new ethers.Contract(USDT_WETH_SUSHI, pair_interface, provider);

let uniswap_price;
let sushiswap_price;

let max_difference = 0;

uniswap_usdt_weth.on("Sync", (a,b) => sync(a,b,p => uniswap_price = p));
sushiswap_usdt_weth.on("Sync", (a,b) => sync(a,b,p => sushiswap_price = p));

function sync (a,b, updater)
{
	const price = parseInt(b._hex, 16) / parseInt(a._hex, 16) * 1e12;

	updater(price);

	const price_difference = (Math.max(uniswap_price, sushiswap_price) / Math.min(uniswap_price, sushiswap_price) - 1) * 100;
	if ( sushiswap_price && uniswap_price)
	{
		const p = document.createElement("p");
		if (price_difference > max_difference)
		{
			p.style.color = "green";
			max_difference = price_difference;
		}
		p.textContent = price_difference;
		document.body.append(p);
	}
}
