// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

interface Token
{
	function transfer (address, uint) external returns (bool);
	function balanceOf (address) external view returns (uint);
	function approve (address, uint) external returns (bool);
}
