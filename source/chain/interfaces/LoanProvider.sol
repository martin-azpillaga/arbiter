// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

interface LoanProvider
{
	function flashLoan (uint baseAmount, uint256 quoteAmount, address to, bytes calldata data) external;
}
