// SPDX-License-Identifier: AGPL-3.0

pragma solidity 0.8.x;

import "../interfaces/LoanProvider.sol";
import "../interfaces/Token.sol";
import "../interfaces/Exchange.sol";

contract Arbiter
{
	address creator;

	constructor ()
	{
		creator = msg.sender;
	}

	function arbiter (LoanProvider provider, uint amount, Token[] calldata tokens, Exchange[] calldata exchanges) external
	{
		provider.flashLoan(0, amount, address(this), abi.encode(provider, amount, tokens, exchanges));
	}

	function DPPFlashLoanCall (address, uint, uint, bytes calldata data) external
	{
		(LoanProvider pool, uint amount, Token[] memory tokens, Exchange[] memory exchanges) = abi.decode(data, (LoanProvider, uint, Token[], Exchange[]));

		for (uint i = 0; i < tokens.length - 1; i++)
		{
			uint balance = tokens[i].balanceOf(address(this));

			tokens[i].approve(address(exchanges[i]), balance);
			exchanges[i].swapExactTokensForTokens(balance, 0, path(tokens[i], tokens[i+1]), address(this), block.timestamp);
		}

		tokens[0].transfer(creator, tokens[0].balanceOf(address(this)) - amount);
		tokens[0].transfer(address(pool), amount);
	}

	function path (Token in_token, Token out_token) private pure returns (address[] memory)
	{
		address[] memory result = new address[](2);
		result[0] = address(in_token);
		result[1] = address(out_token);
		return result;
	}
}
